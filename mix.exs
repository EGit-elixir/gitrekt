defmodule GitRekt.Mixfile do
  use Mix.Project

  def project do
    [
      app: :gitrekt,
      version: "0.3.8",
      elixir: "~> 1.5",
      compilers: [:elixir_make] ++ Mix.compilers,
      make_args: ["--quiet"],
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  def application do
    [extra_applications: [:logger]]
  end

  #
  # Helpers
  #

  defp deps do
    [
      {:elixir_make, "~> 0.6"},
      {:stream_split, "~> 0.1"},
      {:telemetry, "~> 0.4 or ~> 1.0"}
    ]
  end
end
