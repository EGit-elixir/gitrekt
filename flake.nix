{
  description = "Gitrekt";

  outputs = { self, nixpkgs }: let
      systems =
        [ "x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];

      forAllSystems = f: nixpkgs.lib.genAttrs systems (system: f system);

      nixpkgsFor = forAllSystems (system:
        import nixpkgs {
          inherit system;
          #overlays = [ self.overlay ];
        });
    in {

      devShell = forAllSystems (system:
        nixpkgsFor.${system}.mkShell {
          packages = with nixpkgsFor.${system}; [ libgit2 stdenv.cc ];
        });
  };
}
